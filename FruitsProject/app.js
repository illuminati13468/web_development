const { MongoClient } = require("mongodb");
const mongoose = require('mongoose');

mongoose.connect("mongodb://localhost:27017/fruitsDB", {useNewUrlParser: true});

const fruitSchema = new mongoose.Schema ({
  name : String,
  rating: Number,
  review: String
});


const Fruit = mongoose.model("Fruit", fruitSchema);

const personSchema = new mongoose.Schema ({
  name : String,
  age: Number
});

const Person = mongoose.model("Person", personSchema);

const person = new Person ( {
  name: "Sin",
  rating: 37,
});

person.save();


const apple = new Fruit ( {
  name: "Apple",
  rating: 9,
  review: "Not bad!"
});

const kiwi = new Fruit ( {
  name: "Kiwi",
  rating: 10,
  review: "Awesome!!"
});

const raspberry = new Fruit ( {
  name: "Raspberry",
  rating: 9.9,
  review: "Really good!!"
});

Fruit.insertMany([kiwi, raspberry, apple]).then(function() {
  console.log("Successfully saved default items to DB");
})
.catch(function(err) {
  console.log(err);
});